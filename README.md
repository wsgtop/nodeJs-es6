# nodeJs-es6

## 在nodeJS中使用ES6格式

1. 使用npm init -y 创建package.json文件

2. 在项目根文件目录创建文件 .babelrc
- 添加文件内容<br>
{ "presets": [ "es2015" ]}或者{ "presets": ["env"]}  <br>
- bable:  将 ECMAScript 2015 及其版本以后的 javascript 代码转为旧版本浏览器或者是环境中向后兼容版本的  javascript 代码。简而言之，就是把不兼容的 JavaScript 代码转为可兼容，可以执行的 JavaScript 代码。<br>
原始代码 => [babel plugn ] => 转换后的代码
- 需要安装以下几个包<br>
  1. [Babel Preset] 可以视为Babel plugin的集合，例如babel-preset-es2015中包含了所有跟ES6有关的插件，<br>
  2. babel-preset-es2015在.babelrc配置中对于es2015<br>
  3. babel-preset-env在.babelrc配置中对于env<br>
  4. 在代码中运行es6代码的话，需要安装babel-core，<br>
  而如果你想在终端编译es6或者是运行es6版本的REPL的话，需要安装babel-cli<br>
3. 在package.json文件中添加顶级字段“type”，值为”module“
- 添加的意义：以.js结尾或者没有文件后缀名的文件将以ES模块进行加载
- 若没有type字段或type字段类型的值为commonJs， 文件将被视为CommonJS格式
